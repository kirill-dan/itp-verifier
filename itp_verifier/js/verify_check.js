/**
 * Progress bar for create Yandex XML.
 */

(function ($) {
    Drupal.behaviors.VerifyXML = {
        attach: function (context, settings) {
            // Execute code only once.
            if (typeof(init_verify) == "undefined") {
                // Execute code always when you call Ajax.
                init_verify = true;
                var start = Drupal.settings.VerifyXML.start;

                if (start == 1) {
                    StartProgress();
                }

                $('#parsing-verify-check').click(function () {
                    StartProgress();
                });

                function StartProgress() {
                    // Create Progress bar.
                    pb = new Drupal.progressBar('myProgressBar');
                    $(pb.element).appendTo("#progress-bar");
                    pb.setProgress("0", Drupal.t("Processing please wait..."));

                    var i = 1;

                    // Monitoring status script every 100ms.
                    var timerId = setInterval(function () {

                        $.ajax({
                            type: 'POST',
                            url: '/ajax-dynamic-get-progress-verify',
                            dataType: 'json',
                            success: function (Data) {
                                var Obj = Data[1];
                                var percentObj = jQuery.parseJSON(Obj.data);
                                var percent = percentObj.percent;
                                pb.setProgress(percent, Drupal.t("Processing please wait..."));

                                // If export complete.
                                if (percent == 100) {
                                    clearInterval(timerId);

                                    $('#progress-bar').html('<b>' + Drupal.t('Verify emails made successful.') + '</b>');

                                    /*setTimeout(function () {
                                     // Remove progress bar.
                                     $('#progress-bar').html('');
                                     }, 3500);*/
                                }
                            },
                            error: function (result) {
                                //alert('Error');
                            }
                        });
                    }, 100);
                }
            }
        }
    }
})(jQuery);